public class Car {
    private int carID;
    private int timeForCar;
    private int slot;
    private String typeOfCar;
    private String whereVehicleParked;




//КОНСТРУКТОР
    public Car(int carID, String typeOfCar) {
        this.carID = carID;
        this.timeForCar = 0;
        this.slot = 0;
        this.typeOfCar = typeOfCar;
        this.whereVehicleParked = "no where";
    }
//ГЕТТЕРО

    public String getWhereVehicleParked() {
        return whereVehicleParked;
    }

    public String getTypeOfCar() {
        return typeOfCar;
    }

    public int getSlot()  {
        return slot;
    }

    public int getTimeForCar() {
        return timeForCar;
    }

    public int getCarID() {
        return carID;
    }

    //СЕТТЕРО

    public void setSlot(int place) {
        this.slot = place;
    }
    public void setWhereVehicleParked(String whereVehicleParked) {
        this.whereVehicleParked = whereVehicleParked;
    }

    public void setTimeForCar(int timeForCar) {
        this.timeForCar = timeForCar;
    }

    public void setTypeOfCar(String typeOfCar) {
        this.typeOfCar = typeOfCar;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }
}