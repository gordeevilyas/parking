
import java.util.Scanner;
public class Parking {
    private int ParkingSlots;
    private int EmptySlots;
    private int WhenSlotBeEmpty;
    private Car Slots[];
    private String ParkingType;

    //КОНСТРУКТОР
    public Parking(int ParkingSlots, String ParkingType) {
        this.ParkingSlots = ParkingSlots;
        this.EmptySlots = ParkingSlots;
        this.WhenSlotBeEmpty = 0;
        this.Slots = new Car[ParkingSlots];
        this.ParkingType = ParkingType;

    }

    @Override
    public String toString() {
        return "Parking with " + ParkingSlots + " parking slots.";
    }
//ГЕТТЕРО

    public String getParkingType() {
        return ParkingType;
    }

    public int getParkingSlots() {
        return ParkingSlots;
    }

    public int getEmptySlots() {
        return EmptySlots;
    }

    public int getWhenSlotsBeEmpty() {
        return WhenSlotBeEmpty;
    }

    //
    //СЕТТЕРО
    public void setParkingType(String parkingType) {
        ParkingType = parkingType;
    }

    public void setParkingSlots(int parkingSlots) {
        ParkingSlots = parkingSlots;
    }

    public void setEmptySlots(int emptySlots) {
        EmptySlots = emptySlots;
    }

    public void setWhenSlotsBeEmpty(int whenSlotsBeEmpty) {
        WhenSlotBeEmpty = whenSlotsBeEmpty;
    }

    //
    public int returnEmptySlots() {
        for (int j = 0; j < getParkingSlots(); j++) {
            if (Slots[j] == null) {
                return j;
            }
        }
        int k = 0;
        return k;
    }

    public void park(Car car, int time, int n) {
        this.Slots[n] = car;
        this.Slots[n].setTimeForCar(time);
        this.Slots[n].setSlot(n + 1);
        this.Slots[n].setWhereVehicleParked(this.Slots[n].getTypeOfCar());
    }

    public void parkTruckForCar(Car truck, int time, int n) {
        this.Slots[n] = truck;
        this.Slots[n].setTimeForCar(time);
        this.Slots[n].setSlot(n + 1);
        this.Slots[n].setWhereVehicleParked("car");
    }

    public int isEmptyForTruck() {
        if (this.ParkingType.equals("car")) {
            for (int j = 0; j < this.ParkingSlots - 1; j++) {
                if (this.Slots[j] == null) {
                    if (this.Slots[j + 1] == null) {
                        return j;
                    }
                }
            }
            return -1;
        }
        return -1;
    }


    public void clearParking() {
        for (int j = 0; j < ParkingSlots; j++) {
            if (Slots[j] != null) {
                Slots[j].setTimeForCar(0);
                Slots[j].setSlot(0);
                Slots[j] = null;
            }
        }
    }

    public int returnEmptySlot() {
        for (int j = 0; j < getParkingSlots(); j++) {
            if (Slots[j] == null) {
                return j;
            }
        }
        int k = 0;
        return k;
    }

    public void CarParking(Car car, int timeForCar) {
        int k = returnEmptySlot();
        Slots[k] = car;
        Slots[k].setTimeForCar(timeForCar);
        Slots[k].setSlot(k + 1);
    }

    public void RoundEnding() {
        for (int j = 0; j < ParkingSlots; j++) {
            if (Slots[j] != null) {
                Slots[j].setTimeForCar(Slots[j].getTimeForCar() - 1);
            }
        }
    }

    public void updateStatus() {
        int minAvailable = 10;
        int places = 0;
        for (int j = 0; j < ParkingSlots; j++) {
            if (Slots[j] != null) {
                int b = Slots[j].getTimeForCar();
                if (b == 0) {
                    Slots[j].setSlot(0);
                    Slots[j] = null;
                } else if (b <= minAvailable) {
                    minAvailable = b;
                }
            } else {
                places++;
            }
        }
        if (places != 0) {
            setWhenSlotsBeEmpty(0);
        } else {
            setWhenSlotsBeEmpty(minAvailable);
        }
        setEmptySlots(places);
    }
}
