import java.util.Random;
import java.util.Scanner;


public class ParkingLotManager {
    private boolean isOn;
    public ParkingLotManager() {
        this.isOn = true;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }


    public String Instructions() {
        System.out.println((char) 27 + "[31mCommands: " + (char)27 + "[0m");
        System.out.println((char) 27 + "[36mcontinue " + (char)27 + "[0m");
        System.out.println((char) 27 + "[36mstop " + (char)27 + "[0m");
        System.out.println((char) 27 + "[31mAnd also use service commands such as: " + (char)27 + "[0m");
        System.out.println((char) 27 + "[32mstatus " + (char)27 + "[0m");
        System.out.println((char) 27 + "[32mclear " + (char)27 + "[0m");;
        return "______________________________________";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of car slots: ");
        int carSlots = scanner.nextInt();
        Parking carParking = new Parking(carSlots, "car");
        System.out.println("Enter the number of truck slots: ");
        int truckSlots = scanner.nextInt();
        Parking truckParking = new Parking(truckSlots, "truck");
        ParkingLotManager PLM = new ParkingLotManager();
        System.out.println(PLM.Instructions());
        Random random = new Random();
        int id = 1000;
        System.out.println((char) 27 + "[33mEnter Command:  " + (char)27 + "[0m");
        while(PLM.isOn()){
            carParking.updateStatus();
            truckParking.updateStatus();
            switch(scanner.nextLine()){

                case("continue"):{
                    int carAmount = random.nextInt(carSlots/2 + 1);
                    while(carAmount > 0){
                        Car car = new Car(id, "car");
                        if(carParking.getEmptySlots() > 0) {
                            int steps = random.nextInt(10) + 1;
                            int k = carParking.returnEmptySlot();
                            carParking.park(car, steps, k);
                            System.out.println("Car with id " + id + " was parked on place " + car.getSlot() + " for " + car.getTimeForCar() + " steps");
                        }
                        else{
                            System.out.println("There are no car parking slots...");
                            System.out.println("Wait " + carParking.getWhenSlotsBeEmpty() + " steps until it was empty");
                            break;
                        }
                        carAmount--;
                        carParking.updateStatus();
                        id++;
                    }
                    int truckAmount = random.nextInt(truckSlots/2+1);
                    while(truckAmount > 0){
                        Car truck = new Car(id, "truck");
                        if(truckParking.getEmptySlots() > 0) {
                            int steps = random.nextInt(10) + 1;
                            int k = truckParking.returnEmptySlot();
                            truckParking.park(truck, steps, k);
                            System.out.println("Truck with id " + id + " was parked on place " + truck.getSlot() + " for " + truck.getTimeForCar() + " steps");
                        }
                        else if(carParking.isEmptyForTruck() > 0){
                            int steps = random.nextInt(10) + 1;
                            int place = carParking.isEmptyForTruck();
                            carParking.parkTruckForCar(truck, steps, place);
                            carParking.parkTruckForCar(truck, steps, place+1);
                            System.out.println("Trucks with id " + id + " was parked on place " + truck.getSlot() + " and " + (truck.getSlot()+1) + " for " + truck.getTimeForCar() + " steps on car parking");
                        }
                        else{
                            System.out.println("There are no truck parking slots");
                            System.out.println("Wait " + truckParking.getWhenSlotsBeEmpty() + " steps until it was empty");
                            break;
                        }
                        truckAmount--;
                        carParking.updateStatus();
                        truckParking.updateStatus();
                        id++;
                    }
                    carParking.RoundEnding();
                    truckParking.RoundEnding();
                    break;
                }
                case("status"):{
                    System.out.println("Choose parking");
                    String type = scanner.nextLine();
                    switch(type){
                        case("car"):{
                            int x = carParking.getParkingSlots();
                            int y = carParking.getEmptySlots();
                            System.out.println("There are " + x + "  slots in this parking");
                            System.out.println("Empty slots " + y);
                            System.out.println("Not empty slots " + (x-y));
                            if (y == 0) {
                                System.out.println("Available slots after " + carParking.getWhenSlotsBeEmpty() + " steps");
                            }
                            break;
                        }
                        case("truck"):{
                            int x = truckParking.getParkingSlots();
                            int y = truckParking.getEmptySlots();
                            System.out.println("There are " + x + "  slots in this parking");
                            System.out.println("Empty slots " + y);
                            System.out.println("Not empty slots " + (x-y));
                            if (y == 0) {
                                System.out.println("Available slots after " + truckParking.getWhenSlotsBeEmpty() + " steps");
                            }
                            break;
                        }
                    }
                    break;
                }

                case("clear"):{
                    System.out.println("Choose parking");
                    String type = scanner.nextLine();
                    switch(type) {
                        case ("car"): {
                            carParking.clearParking();
                            System.out.println("Car parking cleared");
                            break;
                        }
                        case ("truck"): {
                            truckParking.clearParking();
                            System.out.println("Truck parking cleared");
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
}
